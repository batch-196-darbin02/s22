console.log("hello")

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.
*/
	function register(username) {
		let register = registeredUsers.includes(username);
		if(register === true) {
			alert("Registration failed. Username already exists.");
		} else {
			let register = registeredUsers.push(username);
			alert("Thank you for registering!");
		}
	}

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.
*/

	function addFriend(username) {
		let foundUser = registeredUsers.includes(username);
		if (foundUser === true) {
			let addFriend = friendsList.push(username);
			alert("You have added " + username + " as a friend!");
		} else {
			alert("User not found.");
		}
	}

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

*/
	function showFriends() {
		if(friendsList.length === 0) {
			alert("You currently have 0 friends. Add one first.");
		} else {
			friendsList.forEach(function(){
				console.log();
			})
		}
	}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

	function numberOfFriends(username) {
		if(friendsList.length === 0) {
			alert("You currently have 0 friends. Add one first.");
		} else {
			console.log(friendsList.length);
		}
	}

/*
    5. Create a function which will delete the last item you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.
*/
	function deleteLastFriend (username) {
		if(friendsList.length === 0) {
			alert("You currently have 0 friends. Add one first.");
		} else {
			let deletedFriend = friendsList.pop() 
			console.log(friendsList);
		}
	}